# OpenML dataset: Brilliant-Diamonds

https://www.openml.org/d/43355

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Buying a diamond can be frustrating and expensive.  
It inspired me to create this dataset of 119K natural and lab-created diamonds from brilliantearth.com to demystify the value of the 4 Cs  cut, color, clarity, carat.
This data was scraped using DiamondScraper.
Content



Attribute
Description
Data Type




id
Diamond identification number provided by Brilliant Earth
int


url
URL for the diamond details page
string


shape
External geometric appearance of a diamond
string/categorical


price
Price in U.S. dollars
int


carat
Unit of measurement used to describe the weight of a diamond
float


cut
Facets, symmetry, and reflective qualities of a diamond
string/categorical


color
Natural color or lack of color visible within a diamond, based on the GIA grade scale
string/categorical


clarity
Visibility of natural microscopic inclusions and imperfections within a diamond
string/categorical


report
Diamond certificate or grading report provided by an independent gemology lab
string


type
Natural or lab created diamonds
string


date_fetched
Date the data was fetched
date



Acknowledgements
Thanks to Brilliant Earth for committing to ethically soured jewelry and for having a great shopping experience.  
Check out their buying guide to learn more about diamonds.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43355) of an [OpenML dataset](https://www.openml.org/d/43355). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43355/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43355/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43355/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

